/*
 *  Simple Fractal Renderer in Java
 *  Copyright (C) 2023 Noah Ditzler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.main;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Renderer {
    private final Random random;
    private final BufferedImage image;
    private final Iterator iterator;
    private final Colorizer colorizer;

    private final double centerReal, centerImg;


    double pxSideLength;
    double topLeftReal, topLeftImg;

    Renderer(int imageWidth, int imageHeight, double centerReal, double centerImg,
    double absHeight, Colorizer colorizer, Iterator iterator) {
        this.centerReal = centerReal;
        this.centerImg = centerImg;
        this.colorizer = colorizer;
        this.random = new Random();
        this.iterator = iterator;

        this.pxSideLength = absHeight / ((double) imageHeight);
        this.topLeftReal = centerReal - pxSideLength * (double) (imageWidth / 2);
        this.topLeftImg = centerImg + pxSideLength * (double) (imageHeight / 2);

        image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
    }

    private void renderPixel(int x, int y) {
        //compute complex borders of the area corresponding to the pixel
        double realLeft;
        double imgUpper;
        realLeft = topLeftReal + pxSideLength * (double) x;
        imgUpper = topLeftImg - pxSideLength * (double) y;

        int samples = 5;
        int supersamples = 20;
        boolean supersampling = false;
        List<Color> colors = new ArrayList<Color>();

        for (int i = 0; i < (supersampling ? supersamples : samples); i++) {
            double realOffset = random.nextDouble();
            realOffset = (realOffset - Math.floor(realOffset)) * pxSideLength;
            double imgOffset = random.nextDouble();
            imgOffset = (imgOffset - Math.floor(imgOffset)) * pxSideLength;

            Iterator.IteratorResult result =
                iterator.iterate(realLeft + realOffset, imgUpper - imgOffset);

            colors.add(colorizer.colorize(result));
            if (!supersampling) {
                supersampling = (colors.get(0) != colors.get(i));
            }
        }
        //compute average colour
        int avgRed = 0, avgGreen = 0, avgBlue = 0;

        for (Color c : colors) {
            avgRed += c.getRed();
            avgGreen += c.getGreen();
            avgBlue += c.getBlue();
        }
        avgRed = avgRed / colors.size();
        avgGreen = avgGreen / colors.size();
        avgBlue = avgBlue / colors.size();

        image.setRGB(x, y, new Color(avgRed, avgGreen, avgBlue).getRGB());
    }

    public void saveImage(String path, String filename) {
        File f = new File(path + File.separator + filename);
        if (f == null) {
            throw new RuntimeException("Can't open file at " + path);
        }
        try {
            ImageIO.write(image, "png", f);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    };

    private class RenderThread extends Thread {
        private final Lock l;
        private final Condition done;
        private final int row;
        private boolean isDone;

        RenderThread(int row) {
            l = new ReentrantLock();
            done = l.newCondition();
            this.row = row;
            this.isDone = false;
        }

        void awaitDone() {
            while (true) {
                try {
                    l.lock();
                    if (isDone) {
                        return;
                    }
                    done.await();
                } catch (InterruptedException e) {

                } finally {
                    l.unlock();
                }
            }
        }

        @Override
        public void run() {
            try {
                l.lock();
                for (int ii = 0; ii < image.getWidth(); ii++) {
                    renderPixel(ii, row);
                }
                isDone = true;
                done.signalAll();
            } finally {
                l.unlock();
            }
        }
    }

    public void render() {
        ArrayList<RenderThread> threads = new ArrayList<>();
        for (int i = 0; i < image.getHeight(); i++) {
            RenderThread t = new RenderThread(i);
            threads.add(t);
            t.start();
        }
        for (RenderThread t : threads) {
            t.awaitDone();
        }
    }
}
