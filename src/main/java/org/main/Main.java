/*
*  Simple Fractal Renderer in Java
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package org.main;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Main {
    public static void main(String[] args) {
        int width = 1920, height = 1080;
        int maxIterations = 100;
        Colorizer colorizer = new DefaultColorizer(maxIterations);
        Iterator iterator = new MandelbrotIterator(maxIterations);
        Renderer r = new Renderer(width, height, -0.78, 0.15, 0.08, colorizer, iterator);


        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Portable Network Graphic (.png)", "png");
        fileChooser.setFileFilter(filter);
        fileChooser.setDialogTitle("Select a file to output image.");
        int success;
        do {
            success = fileChooser.showSaveDialog(null);
        } while (success != JFileChooser.APPROVE_OPTION);

        r.render();

        r.saveImage(fileChooser.getSelectedFile().getParent(),
                fileChooser.getSelectedFile().getName());
        JOptionPane.showMessageDialog(null, "Saved file at " + fileChooser.getSelectedFile().getPath());
    }
}
