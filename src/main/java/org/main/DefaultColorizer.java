/*
 *  Simple Fractal Renderer in Java
 *  Copyright (C) 2023 Noah Ditzler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.main;

import java.awt.*;

public class DefaultColorizer implements Colorizer {
    private final int maxIterations;
    DefaultColorizer(int maxIterations) {
        this.maxIterations = maxIterations;
    }
    @Override
    public Color colorize(Iterator.IteratorResult result) {
        if (result.resultType == Iterator.ResultType.CONVERGES) {
            return new Color(0, 0, 0);

        }
        return Color.getHSBColor((float) result.iterations / (float) maxIterations,
            1.0f - 1.0f / result.iterations,
            1.0f - 1.0f / result.iterations);
        /*
        return new Color(0,
                Math.min(255, result.iterations * result.iterations),
                Math.min(255, result.iterations * result.iterations * result.iterations));

         */
    }
}
