/*
 *  Simple Fractal Renderer in Java
 *  Copyright (C) 2023 Noah Ditzler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.main;

public abstract class Iterator {
    enum ResultType {
        DIVERGES,
        CONVERGES
    }
    class IteratorResult {
        public final int iterations;
        public final ResultType resultType;
        private IteratorResult(ResultType resultType, int iterations) {
            this.resultType = resultType;
            this.iterations = iterations;
        };
    }
    IteratorResult[] possibleResults;
    private int maxIterations;
    public Iterator(int maxIterations) {
        this.maxIterations = maxIterations;
        possibleResults = new IteratorResult[maxIterations + 1];
        possibleResults[0] = new IteratorResult(ResultType.CONVERGES, 0);
        for (int i = 1; i <= maxIterations; i++) {
            possibleResults[i] = new IteratorResult(ResultType.DIVERGES, i);
        }
    }

    protected Pair<Double, Double> step(double zreal, double zimg, double creal, double cimg) {
        return new Pair<>(zreal * zreal - zimg * zimg + creal,  2 * zreal * zimg + cimg);
    }

    protected abstract Pair<Pair<Double, Double>, Pair<Double, Double>> init(double real, double img);

    public IteratorResult iterate(double real, double img) {
        double zreal, zimg, creal, cimg;
        //Java was not a good choice for this
        //Could probably speed up by reusing an object but it would be ugly
        Pair<Pair<Double, Double>, Pair<Double, Double>> init = init(real, img);

        zreal = init.l.l;
        zimg = init.l.r;
        creal = init.r.l;
        cimg = init.r.r;

        for (int i = 0; i < maxIterations; i++) {
            Pair<Double, Double> res = step(zreal, zimg, creal, cimg);
            zreal = res.l;
            zimg = res.r;
            if (zreal * zreal + zimg * zimg >= 4) {
                return possibleResults[i + 1];
            }
        }
        return possibleResults[0];
    }
}
